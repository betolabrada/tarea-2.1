﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movimiento : MonoBehaviour {

    // main player logic
    // movement and rotation
    // health: player is instantly killed by contact with any enemy(dummys, pilar, bomb)
    // there is also a couritine of time left it is stopped when game is finished
    public float speed = 6f;
    public Camera cameraPlayer;
    public Text timerText;
    public AudioClip tock;
    public AudioClip tick;
    public AudioClip explosion;
    Spawning spawning;

    int second;

    Health pilarHealth;
    Animator anim;
    Vector3 movement;
    Rigidbody playerRigidbody;
    AudioSource playerAudio;
    BoxCollider playerBoxCollider;

    bool estaMuerto = false;
    bool flagPilar = false;

	void Awake()
    {
        second = 60;
        playerRigidbody = GetComponent<Rigidbody>();
        playerAudio = GetComponent<AudioSource>();
        playerBoxCollider = GetComponent<BoxCollider>();
        spawning = GameObject.Find("Cube").GetComponent<Spawning>();
        anim = GetComponent<Animator>();
        StartCoroutine("Seconds");

    }

    void FixedUpdate () {

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Rotate();

        if (spawning.spawnedPilar)
        {
            if (!flagPilar)
            {
                pilarHealth = GameObject.Find("Pilar(Clone)").GetComponent<Health>();
                flagPilar = true;
            }
            if (pilarHealth.estaMuerto)
            {
                StopGame();
                spawning.GameOver(true);
            }


        }

        if (second < 0 && !spawning.gameOver)
        {
            spawning.GameOver(false);
        }
	}

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        // normalize, so its value when both axis are pressed is always 1
        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Rotate()
    {
        Ray camRay = cameraPlayer.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit))
        {
            //print("hit: " + floorHit.point);
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRot = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRot);
        }
        else
        {
            //print("no hit");
        }
    }

    void StopGame()
    {
        StopCoroutine("Seconds");
        playerAudio.clip = explosion;
        playerAudio.Play();

        if (!estaMuerto)
        {
            Muerte();
        }
    }

    void Muerte()
    {
        estaMuerto = true;
        playerBoxCollider.enabled = false;
        anim.SetTrigger("Dead");
        this.enabled = false;

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag != "Bullet" && other.gameObject.name != "Cube")
        {
            Destroy(other.gameObject);
            StopGame();
            spawning.GameOver(false);
        }
    }

    IEnumerator Seconds()
    {
        while (second >= 0)
        {
            playerAudio.clip = second % 2 == 0 ? tick : tock;
            playerAudio.Play();
            timerText.text = "" + second;
            second--;
            yield return new WaitForSeconds(1f);
        }
    }
}
