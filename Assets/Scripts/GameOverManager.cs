﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour {

    // triggers game over animation
    // sets a timer and restarts scene
    Animator anim;
    Spawning sp;

    float restartDelay = 5f;
    float timer;
    void Awake()
    {
        anim = GetComponent<Animator>();
        sp = GameObject.Find("Cube").GetComponent<Spawning>();
    }

    void Update()
    {
        if (sp.gameOver)
        {
            timer += Time.deltaTime;

            if (timer >= restartDelay)
            {
                SceneManager.LoadScene(0);
            }
            anim.SetTrigger("GameOver");
        }
    }
}
