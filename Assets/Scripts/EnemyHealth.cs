﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {

    // health of tanks
    // the only way they can be harmed is with bullets
    // triggers animation when dead then it is destroyed after 3 seconds
    public int saludInicial = 3;
    public int saludActual;

    EnemyMov enemyMov;

    bool danio;
    public bool estaMuerto;
    AudioSource playerAudio;

    Animator anim;

    void Start()
    {
        playerAudio = GetComponent<AudioSource>();
        saludActual = saludInicial;
        estaMuerto = false;

        anim = GetComponent<Animator>();
        enemyMov = GetComponent<EnemyMov>();
    }

    void Update()
    {
        danio = false;
    }
    public void HacerDanio(int cantidad)
    {
        danio = true;

        saludActual -= cantidad;

        playerAudio.Play();

        if (saludActual <= 0 && !estaMuerto)
        {
            Muerte();
        }
    }

    void Muerte()
    {
        estaMuerto = true;

        enemyMov.enabled = false;

        anim.SetTrigger("Dead");

        Destroy(gameObject, 3f);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Bullet")
        {
            HacerDanio(1);
        }
    }
}
