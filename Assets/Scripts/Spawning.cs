﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawning : MonoBehaviour {


    // spawns bombs from the sky
    // spawns pilar after the two tanks are destroyed
    // game over logic: plays success or failure clip and sets text of final UI
    public GameObject bomba;
    public GameObject pilar;
    public AudioClip gameOverClip;
    public AudioClip wonClip;
    public Text resultText;

    EnemyHealth enemy1;
    EnemyHealth enemy2;

    float timer;
    Vector3 randomPos;
    Vector3 randomCorner;

    float[] xRange;
    float[] zRange;

    public bool spawnedPilar = false;
    // this is a flag which tells us if the game has finished
    public bool gameOver = false;

    AudioSource playerAudio;
    // Use this for initialization
    void Start() {
        enemy1 = GameObject.Find("TankDummy").GetComponent<EnemyHealth>();
        enemy2 = GameObject.Find("TankDummy (1)").GetComponent<EnemyHealth>();

        playerAudio = GetComponent<AudioSource>();
        playerAudio.Play();

        xRange = new float[] { -15f, 15f };
        zRange = new float[] { -15f, 15f };

    }

    // Update is called once per frame
    void Update() {
        timer += Time.deltaTime;
        if (timer > 3f)
        {
            SetRandomPos();
            SpawnBomb();
        }
        if (enemy1.estaMuerto && enemy2.estaMuerto && !spawnedPilar)
        {
            SetRandomCorner();
            SpawnGoal();
            spawnedPilar = true;
        }

        //if (player.estaMuerto && !gameOver)
        //{
        //    GameOver(false);
        //}
    }

    void SetRandomPos()
    {
        // random x
        float randomX = Random.Range(xRange[0], xRange[1]);
        float x = Mathf.Clamp(randomX, xRange[0], xRange[1]);
        // random z
        float randomZ = Random.Range(zRange[0], zRange[1]);
        float z = Mathf.Clamp(randomZ, zRange[0], zRange[1]);

        randomPos.Set(x, 20f, z);
    }

    void SetRandomCorner()
    {
        float[] corners = new float[] { 19f, -19f };
        int x = Random.Range(0, corners.Length);
        int z = Random.Range(0, corners.Length);

        randomCorner.Set(corners[x], 1.35f, corners[z]);

        // reduce bomb zone
        if (corners[x] > 0)
        {
            xRange[0] = corners[x] / 2;
            xRange[1] = corners[x];
        }
        else
        {
            xRange[0] = corners[x];
            xRange[1] = corners[x] / 2;
        }

        if (corners[z] > 0)
        {
            zRange[0] = corners[z] / 2;
            zRange[1] = corners[z];
        }
        else
        {
            zRange[0] = corners[z];
            zRange[1] = corners[z] / 2;
        }
    }

    void SpawnBomb()
    {
        timer = 0f;
        Instantiate(bomba, randomPos, Quaternion.identity);
    }

    void SpawnGoal()
    {
        Instantiate(pilar, randomCorner, Quaternion.identity);
    } 

    public void GameOver(bool won)
    {
        print("called");
        playerAudio.Stop();
        playerAudio.clip = won ? wonClip : gameOverClip;
        playerAudio.Play();
        gameOver = true;

        resultText.text = won ? "You won!" : "You lost!";
       
    }

}
