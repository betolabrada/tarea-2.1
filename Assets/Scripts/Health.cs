﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {

    // health of pilar
    // an UI which tells us the current health

    public int saludInicial = 10;
    public int saludActual;

    Spawning spawning;

    bool danio;
    public bool estaMuerto;

    public Image healthBar;

    void Start()
    {
        saludActual = saludInicial;
        estaMuerto = false;

        spawning = GameObject.Find("Cube").GetComponent<Spawning>();
    }

    void Update()
    {
        danio = false;
    }
    public void HacerDanio(int cantidad)
    {
        danio = true;

        saludActual -= cantidad;

        healthBar.fillAmount = (saludActual * 1f ) / saludInicial;

        if (saludActual <= 0 && !estaMuerto)
        {
            Muerte();
        }
    }

    void Muerte()
    {
        estaMuerto = true;
        spawning.GameOver(true);
        Destroy(gameObject, 3f);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Bullet")
        {
            HacerDanio(1);
        }
    }
}
