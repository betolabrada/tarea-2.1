﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMov : MonoBehaviour {

    // patroling of the two tanks
    public Nodo[] path;
    private float rango = 0.5f;

    private int actual;

    // Use this for initialization
    void Start() {
        actual = 0;
        StartCoroutine(ChecarSiLlegue());
    }

    // Update is called once per frame
    void Update() {
        transform.LookAt(path[actual].transform);
        transform.Translate(transform.forward * Time.deltaTime * 5, Space.World);
    }

    IEnumerator ChecarSiLlegue()
    {
        float d;
        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            d = Vector3.Distance(
                transform.position,
                path[actual].transform.position
                );
            if (d < rango)
            {
                actual++;
                actual %= path.Length;
            }

        }
    }
}
