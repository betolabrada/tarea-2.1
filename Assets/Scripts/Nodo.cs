﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nodo : MonoBehaviour {

    public Nodo[] vecinos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // solo funciona en editor
    void OnDrawGizmos()
    {
        // logica para dibujar en gizmos
        // desarrollo
        // frustum
        Gizmos.color = Color.red;

        //Gizmos.DrawSphere(transform.position, 1f);
        Gizmos.DrawSphere(transform.position, 1f);

        Gizmos.color = Color.blue;
        for (int i = 0; i < vecinos.Length; i++)
        {
            Gizmos.DrawLine(
                transform.position,
                vecinos[i].transform.position
                );
        }
        
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(30, 193, 252);
        Gizmos.DrawWireSphere(transform.position, 1f);
    }
}
