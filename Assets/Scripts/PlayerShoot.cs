﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

    // player shooting logic
    // clicking instantiates bullets
    public GameObject original;
    public float range = 100f;

    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask;



    float timer = 0f;

    void Awake()
    {
        shootRay = new Ray();
        shootableMask = LayerMask.GetMask("Shootable");
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
		if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
	}


    void Shoot()
    {

        //Instantiate(original, transform.position, original.transform.rotation);


        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        GameObject inst = Instantiate(original, transform.position, Quaternion.identity);
        Destroy(inst, 5f);

        if (Physics.Raycast(shootRay, out shootHit))
        {
            print("le di a enemigo");
        }

    }
}
