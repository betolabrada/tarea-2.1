﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

    // bala logic, with this gameObject you take damage to enemies
    GameObject tank;
    Rigidbody balaRigidbody;
    Vector3 shootVec;

	// Use this for initialization
	void Start () {
        tank = GameObject.Find("Tank");
        balaRigidbody = GetComponent<Rigidbody>();
        print(tank.transform.eulerAngles.y);
        shootVec = transform.forward;
        shootVec = Quaternion.Euler(0f, tank.transform.eulerAngles.y, 0f) * shootVec;
       
        balaRigidbody.AddForce(shootVec * 10, ForceMode.Impulse);

        
    }
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.layer == 9)
            Destroy(gameObject);
    }

}
